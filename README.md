# MapScriptTools for Civilization IV #

**Forum thread:** http://forums.civfanatics.com/showthread.php?t=540261

MapScriptTools provides tools to facilitate the creation of MapScripts and adapting existing ones for Civilization IV Beyond the Sword. It helps on making MapScripts compatible for 'Normal', 'Fall from Heaven 2', 'Planetfall' and 'Mars Now!' mods. It is not intended to work with 'Final Frontier' based mods.


It also includes a selection of MapScripts already adapted to make use of these features. All included MapScripts can be used with the mods mentioned before, and they have a detailed changelog in the initial part of each file that explains all changes and new features.


MapScriptTools was originally created by Temudjin. This new version has been made in order to incorporate bugfixes and some new features to these excellent tools. The original version can be found here: http://forums.civfanatics.com/showthread.php?t=371816